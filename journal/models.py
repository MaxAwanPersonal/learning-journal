# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.


class Resource(models.Model):

    name = models.CharField(max_length=256, blank=False, null=False)
    link = models.URLField(blank=True, null=True)
    language = models.ForeignKey('Language', blank=True, null=True)
    framework = models.ForeignKey('Framework', blank=True, null=True)
    frontend = models.ForeignKey('FrontEnd', blank=True, null=True)
    database = models.ForeignKey('Database', blank=True, null=True)
    library = models.ForeignKey('Library', blank=True, null=True)
    technology = models.ForeignKey('Technology', blank=True, null=True)
    attachment = models.FileField(default=None, blank=True, null=True)


class Language(models.Model):

    name = models.CharField(max_length=128, blank=False, null=False)

    def __str__(self):
        return self.name


class Framework(models.Model):

    name = models.CharField(max_length=128, blank=False, null=False)

    def __str__(self):
        return self.name


class Database(models.Model):

    name = models.CharField(max_length=128, blank=False, null=False)
    def __str__(self):
        return self.name


class Library(models.Model):

    name = models.CharField(max_length=128, blank=False, null=False)
    def __str__(self):
        return self.name


class Technology(models.Model):

    name = models.CharField(max_length=128, blank=False, null=False)
    def __str__(self):
        return self.name


class FrontEnd(models.Model):

    name = models.CharField(max_length=128, blank=True, null=False)

    def __str__(self):
        return self.name