from django.forms import ModelForm
from .models import Resource, Language, Framework, Database, Library, Technology, FrontEnd


class ResourceForm(ModelForm):
    class Meta:
        model = Resource
        fields = ['name', 'link', 'attachment', 'language', 'framework', 'frontend', 'database', 'library', 'technology' ]


class LanguageForm(ModelForm):
    class Meta:
        model = Language
        fields = ['name']


class FrameworkForm(ModelForm):
    class Meta:
        model = Framework
        fields = ['name']


class DatabaseForm(ModelForm):
    class Meta:
        model = Database
        fields = ['name']


class LibraryForm(ModelForm):
    class Meta:
        model = Library
        fields = ['name']


class TechnologyForm(ModelForm):
    class Meta:
        model = Technology
        fields = ['name']


class FrontEndForm(ModelForm):
    class Meta:
        model = FrontEnd
        fields = ['name']