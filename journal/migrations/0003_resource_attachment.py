# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-07-17 10:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('journal', '0002_auto_20180713_1526'),
    ]

    operations = [
        migrations.AddField(
            model_name='resource',
            name='attachment',
            field=models.FileField(blank=True, default=None, null=True, upload_to=b''),
        ),
    ]
