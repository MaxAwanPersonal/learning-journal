from django.conf import settings
from django.conf.urls import include, url
from journal import views

urlpatterns = [
    url(r'^$', views.view_resources, name='resources'),
    url(r'^search/(?P<search_term>[\w ]+)$', views.view_resources, name='resources'),
    url(r'^languages/$', views.LanguageListView.as_view(), name='languages'),
    url(r'^frameworks/$', views.FrameworkListView.as_view(), name='frameworks'),
    url(r'^databases/$', views.DatabaseListView.as_view(), name='databases'),
    url(r'^libraries/$', views.LibraryListView.as_view(), name='libraries'),
    url(r'^technologies/$', views.TechnologyListView.as_view(), name='technologies'),
    url(r'^frontends/$', views.FrontEndListView.as_view(), name='frontends'),
    url(r'^resource/new$', views.create_resource, name='create_resource'),
    url(r'^language/$', views.LanguageView.as_view(), name='language'),
    url(r'^language/(?P<id>[0-9]+)$', views.LanguageView.as_view(), name='language'),
    url(r'^framework/$', views.FrameworkView.as_view(), name='framework'),
    url(r'^framework/(?P<id>[0-9]+)$', views.FrameworkView.as_view(), name='framework'),
    url(r'^database/$', views.DatabaseView.as_view(), name='database'),
    url(r'^database/(?P<id>[0-9]+)$', views.DatabaseView.as_view(), name='database'),
    url(r'^library/$', views.LibraryView.as_view(), name='library'),
    url(r'^library/(?P<id>[0-9]+)$', views.LibraryView.as_view(), name='library'),
    url(r'^technology/$', views.TechnologyView.as_view(), name='technology'),
    url(r'^technology/(?P<id>[0-9]+)$', views.TechnologyView.as_view(), name='technology'),
    url(r'^frontend/$', views.FrontEndView.as_view(), name='frontend'),
    url(r'^frontend/(?P<id>[0-9]+)$', views.FrontEndView.as_view(), name='frontend'),
    url(r'^resource/(?P<id>[0-9]+)', views.edit_resource, name='edit_resource'),
]