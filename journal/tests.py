# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase, RequestFactory
from .models import Resource, Language, Framework
from .views import view_resources
from .forms import ResourceForm
# Create your tests here.


class TestResourcesShow(TestCase):

    def setUp(self):
        Resource.objects.create(name="facebook", link="http://www.facebook.com")
        self.factory = RequestFactory()

    def test_resources_show(self):
        request = self.factory.get('/')
        response = view_resources(request)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'facebook')
        self.assertContains(response, 'http://www.facebook.com')


class TestNewResources(TestCase):

    def setUp(self):
        self.resource_data = {
            "name": "reddit",
            "link": "http://www.reddit.com",
        }
        self.res_form = ResourceForm(self.resource_data)

    def test_resource(self):
        self.assertTrue(self.res_form.is_valid())
        resource = self.res_form.save()
        self.assertEqual(resource.name, 'reddit')
        self.assertEqual(resource.link, 'http://www.reddit.com')


class TestNewResourceValidation(TestCase):

    def setUp(self):
        self.resource_data = {
            "name": "",
            "link": ""
        }

    def test_required_fields_for_new_resource(self):
        res_form = ResourceForm(self.resource_data)
        res_form.is_valid()
        self.assertIn('name', res_form.errors)
        self.assertNotIn('link', res_form.errors)


class TestSearch(TestCase):

    def setUp(self):
        lng = Language.objects.create(name="python")
        frm = Framework.objects.create(name="django")
        Resource.objects.create(name="docs", link="https://docs.djangoproject.com/en", language=lng, framework=frm)
        self.search_data = {
            "search_term": "PYTHON",
        }
        self.factory = RequestFactory()

    def test_search_func(self):
        request = self.factory.post('/', self.search_data)
        response = view_resources(request)
        self.assertContains(response, 'PYTHON')
        self.assertContains(response, 'docs')
        self.assertContains(response, 'https://docs.djangoproject.com/en')
