# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.views import View
from .models import Resource, Language, Framework, Database, Library, Technology, FrontEnd
from .forms import ResourceForm, LanguageForm, FrameworkForm, DatabaseForm, LibraryForm, TechnologyForm, FrontEndForm
from django.shortcuts import render, redirect, get_object_or_404
from django.core.urlresolvers import reverse
from django.db.models import Q


def view_resources(request, search_term=None):
    resources = Resource.objects.all()
    if request.method == 'POST':
        search_term = request.POST.get('search_term', None)
    if search_term:
        resources = resources.filter(Q(name__icontains=search_term) | Q(link__icontains=search_term) | Q(language__name__icontains=search_term) | Q(framework__name__icontains=search_term) | Q(frontend__name__icontains=search_term) | Q(database__name__icontains=search_term) | Q(library__name__icontains=search_term) | Q(technology__name__icontains=search_term))

    return render(request, 'resources.html', {
        'resources': resources,
        'search_term': search_term,
    })


def create_resource(request):
    if request.method == 'POST':
        resource_form = ResourceForm(request.POST, request.FILES)
        if resource_form.is_valid():
            resource_form.save()
            return redirect(reverse('journal:resources'))
    else:
        resource_form = ResourceForm()
        return render(request, 'new_resource.html', {'resource_form': resource_form, 'adding': True})


def edit_resource(request, id):
    resource = get_object_or_404(Resource, id=id)
    if request.method == 'POST':
        resource_form = ResourceForm(request.POST, request.FILES, instance=resource)
        if resource_form.is_valid():
            resource_form.save()
            return redirect('journal:resources')
    else:
        resource_form = ResourceForm(instance=resource)
    return render(request, 'new_resource.html', {
        'id': resource.id,
        'resource_form': resource_form,
        'adding': False,
    })


class ItemsView(View):
    model = Resource
    template = 'list.html'
    iterable_name = 'items'
    view_link = None

    def get(self, request):
        items = self.model.objects.all()
        return render(request, self.template, {
            'view_link': self.view_link,
            self.iterable_name: items,
        })


class BaseView(View):

    form = None
    template = 'base_form.html'
    return_url = None
    model = Resource
    item_name = "resource"

    def get(self, request, *args, **kwargs):
        obj_id = kwargs.get('id', None)
        if obj_id:
            inst = get_object_or_404(self.model, id=obj_id)
            form = self.form(instance=inst)
            adding=False
        else:
            form = self.form()
            adding=True
        return render(request, self.template, {
            'id': obj_id,
            'form': form,
            'item_name': self.item_name,
            'adding': adding,
        })

    def post(self, request, *args, **kwargs):
        obj_id = kwargs.get('id', None)
        if obj_id:
            inst = get_object_or_404(self.model, id=obj_id)
            form = self.form(request.POST, instance=inst)
        else:
            form = self.form(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse(self.return_url))


class LanguageView(BaseView):
    form = LanguageForm
    model = Language
    return_url = "journal:languages"
    item_name = "language"


class LanguageListView(ItemsView):

    model = Language
    view_link = "journal:language"


class FrameworkView(BaseView):
    form = FrameworkForm
    model = Framework
    return_url = 'journal:frameworks'
    item_name = "framework"

class FrameworkListView(ItemsView):

    model = Framework
    view_link = "journal:framework"


class DatabaseView(BaseView):
    form = DatabaseForm
    model = Database
    return_url = 'journal:databases'
    item_name = "database"

class DatabaseListView(ItemsView):

    model = Database
    view_link = "journal:database"


class LibraryView(BaseView):
    form = LibraryForm
    model = Library
    return_url = 'journal:libraries'
    item_name = "library"

class LibraryListView(ItemsView):

    model = Library
    view_link = "journal:library"


class TechnologyView(BaseView):
    form = TechnologyForm
    model = Technology
    return_url = 'journal:technologies'
    item_name = "technology"


class TechnologyListView(ItemsView):

    model = Technology
    view_link = "journal:technology"


class FrontEndView(BaseView):
    form = FrontEndForm
    model = FrontEnd
    return_url = 'journal:frontends'
    item_name = "framework"


class FrontEndListView(ItemsView):

    model = FrontEnd
    view_link = "journal:frontend"